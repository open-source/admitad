document.onreadystatechange = () => {
    if(document.readyState === 'complete'){
        document.querySelector('#filter').onkeyup = (e) => {
            filter();
        }
        filter();

        document.querySelectorAll('.program').forEach((element) => {
            element.onclick = (e) => {
                document.location.href = element.getAttribute("data-href")
            }
        });
    }
}

function filter() {
    let text = document.querySelector("#filter").value.toLowerCase();
    document.querySelectorAll('.program').forEach((element) => {
        let elementText = element.innerText.toLowerCase();
        if(elementText.includes(text)){
            element.style.display = "grid";
        }else{
            element.style.display = "none";
        }
    });
}