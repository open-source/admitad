<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Admitad;

class AdmitadOauth extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Models\Admitad', function($app){
            return new Admitad();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
