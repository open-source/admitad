<?php

return [
    'website_token' => env("WEBSITE_TOKEN", "weak_password"),
    "auth" => [
        "client_id" => env("ADMITAD_CLIENT_ID", ""),
        "client_secret" => env("ADMITAD_CLIENT_SECRET", ""),
    ],
    "geoip" => [
        "v4_download" => env("GEOIP_V4_DOWNLOAD", ""),
        "v6_download" => env("GEOIP_V6_DOWNLOAD", ""),
    ]
];
