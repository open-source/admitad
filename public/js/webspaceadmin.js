/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!***************************************!*\
  !*** ./resources/js/webspaceadmin.js ***!
  \***************************************/
document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    document.querySelector('#filter').onkeyup = function (e) {
      filter();
    };

    filter();
    document.querySelectorAll('.program').forEach(function (element) {
      element.onclick = function (e) {
        document.location.href = element.getAttribute("data-href");
      };
    });
  }
};

function filter() {
  var text = document.querySelector("#filter").value.toLowerCase();
  document.querySelectorAll('.program').forEach(function (element) {
    var elementText = element.innerText.toLowerCase();

    if (elementText.includes(text)) {
      element.style.display = "grid";
    } else {
      element.style.display = "none";
    }
  });
}
/******/ })()
;