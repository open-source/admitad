<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Active Partner Programs</title>
    <link rel="stylesheet" href="{{mix('css/webspaceadmin.css')}}">
    <script src="{{mix('js/webspaceadmin.js')}}" async></script>
</head>
<body>
    <main>
        <h1>Active Partner Programs</h1>
        <input type="text" name="filter" id="filter" placeholder="Filter..." autofocus>
        <div id="programs">
        @foreach($programs as $program)
            <div class="program card" data-href="{{route('adspaceadmin-id', ["id" => $program["id"]])}}">
                <h4>{{$program["name"]}}</h4>
                <img src="{{$program['image']}}" alt="Program Image">
                <a href="{{$program['url']}}" target="_blank" rel="noopener noreferrer">{{$program['url']}}</a>
            </div>
        @endforeach
        </div>
    </main>
</body>
</html>