<?php

namespace App\Models;

use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Log;

class Admitad
{
    const API_URL_PROGRAMS = "https://api.admitad.com/advcampaigns/website/"; # Needs to be appended by adspace id
    const API_ACCESSKEY = "https://api.admitad.com/token/";
    const API_DEEPLINKGENERATOR_URL = "https://api.admitad.com/deeplink/{w_id}/advcampaign/{c_id}/";
    const API_REFRESH_KEY_REDIS = "admitad.refreshlock";
    const AD_SPACES = [
        "de" => "1781858",
        "en" => "1781859"
    ];

    public function __construct()
    {
    }

    public function fetchPrograms()
    {
        /**
         * Array containing following items:
         * access_token,
         * refresh_token,
         * expires_in
         */
        $auth = $this->getAccessKey();
        $programs = [];
        foreach (self::AD_SPACES as $website => $id) {
            $url = self::API_URL_PROGRAMS . $id . "/";

            $params = [
                "connection_status" => "active",
                "has_tool" => "deeplink",
                "limit" => 50,
                "offset" => 0,
            ];


            // Fetch all Programs for this adspace
            while (true) {
                $fetch_url = $url . "?" . http_build_query($params, "", "&", PHP_QUERY_RFC3986);

                $context = stream_context_create([
                    "http" => [
                        "method" => "GET",
                        "header" => [
                            "Authorization: Bearer " . $auth["access_token"]
                        ],
                        "ignore_errors" => true
                    ]
                ]);

                $result = file_get_contents($fetch_url, false, $context);


                if ($result === false) {
                    Log::error("Failure fetching " . $fetch_url);
                    break;
                }

                $result = json_decode($result, true);

                if (empty($result) || empty($result["_meta"]) || empty($result["results"])) {
                    Log::error("Aborting " . $url . " returned " . json_encode($result));
                    break;
                }

                $meta = $result["_meta"];

                foreach ($result["results"] as $res) {
                    $siteUrl = $res["site_url"];

                    $host = \App\Models\Admitad::parseHost($siteUrl);
                    if (!empty($host)) {
                        $programs[$website][$host] = $res;
                    }
                }

                Log::info("[" . $website . "] Fetched " . min($meta["offset"] + $meta["limit"], $meta["count"]) . "/" . $meta["count"]);
                if ($meta["offset"] + $meta["limit"] >= $meta["count"]) {
                    break;
                } else {
                    $params["offset"] += $meta["limit"];
                }
            }
        }
        return $programs;
    }

    /**
     * Access Key will be stored in Redis
     * admitad => [
     *   access_token => <ACCESS_KEY>
     *   refresh_token => <REFRESH_KEY>
     *   expires_in => <VALID_UNTIL>
     * ]
     */
    public function getAccessKey($rec = 0)
    {
        if ($rec > 20) {
            return null;
        }
        // Central shared Redis Host
        $redis = Redis::connection(config("cache.stores.redis.connection"));
        $authorization = $redis->hgetall("admitad");
        if (empty($authorization)) {
            if (config("app.env") !== "production") {
                // Try to fetch an active access_key from Live Instance
                $url = "https://direct.metager.de/access-token";
                $context = stream_context_create([
                    "http" => [
                        "method" => "GET",
                        "header" => [
                            "Authorization: Bearer " . config("admitad.website_token"),
                        ],
                        "ignore_errors" => false,
                    ]
                ]);
                try {
                    $token = file_get_contents($url, false, $context);
                    $token = json_decode($token, true);
                    if (!empty($token) && isset($token["error"]) && !$token["error"] && !empty($token["result"])) {
                        $this->storeAccessToken($token["result"]);
                        return $this->getAccessKey($rec + 1);
                    }
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
            if (!$this->getRefreshKeyLock()) {
                sleep(1);
                return $this->getAccessKey($rec + 1);
            }
            $this->createNewAccessKey();
            $redis->del(self::API_REFRESH_KEY_REDIS);
            return $this->getAccessKey($rec + 1);
        }

        // We will check if the Key is about to expire
        $expiresIn = Carbon::createFromFormat("d.m.Y H:i:s", $authorization["expires_in"]);

        if (Carbon::now()->diffInHours($expiresIn) < 12) {
            // We need to refresh our key
            if (!$this->getRefreshKeyLock()) {
                sleep(1);
                return $this->getAccessKey($rec + 1);
            }
            $this->refreshAccessKey($authorization);
            $redis->del(self::API_REFRESH_KEY_REDIS);

            return $this->getAccessKey($rec + 1);
        }
        return $authorization;
    }

    private function refreshAccessKey($authorization)
    {
        $redis = Redis::connection(config("cache.stores.redis.connection"));
        Log::info("Refreshing Access Key");
        $params = [
            "client_id" => config("admitad.auth.client_id"),
            "client_secret" => config("admitad.auth.client_secret"),
            "refresh_token" => $authorization["refresh_token"],
            "grant_type" => "refresh_token"
        ];
        $context = stream_context_create([
            "http" => [
                "method" => "POST",
                "header" => [
                    "Authorization: Basic " . base64_encode(config("admitad.auth.client_id") . ":" . config("admitad.auth.client_secret")),
                    "Content-Type: application/x-www-form-urlencoded"
                ],
                "content" => http_build_query($params, "", "&", PHP_QUERY_RFC3986),
                "ignore_errors" => true,
                "protocol_version" => "1.2",
            ]
        ]);
        $result = file_get_contents(self::API_ACCESSKEY . "?" . http_build_query($params, "", "&", PHP_QUERY_RFC3986), false, $context);
        if (empty($result) || empty($result["access_token"]) || empty($result["expires_in"]) || empty($result["refresh_token"])) {
            if (!empty($result)) {
                // Something is wrong with our refresh key
                // Create a new Key instead
                return $this->createNewAccessKey();
            }
            return false;
        }

        $result["expires_in"] = Carbon::now()->addSeconds($result["expires_in"])->format("d.m.Y H:i:s");
        $this->storeAccessToken($result);
    }

    private function createNewAccessKey()
    {
        $redis = Redis::connection(config("cache.stores.redis.connection"));
        Log::info("Creating new Access Key");
        // There is no Accesskey yet Generate a new one
        $params = [
            "client_id" => config("admitad.auth.client_id"),
            "scope" => "advcampaigns_for_website deeplink_generator",
            "grant_type" => "client_credentials"
        ];
        $context = stream_context_create([
            "http" => [
                "method" => "POST",
                "header" => [
                    "Authorization: Basic " . base64_encode(config("admitad.auth.client_id") . ":" . config("admitad.auth.client_secret")),
                    "Content-Type: application/x-www-form-urlencoded"
                ],
                "content" => http_build_query($params, "", "&", PHP_QUERY_RFC3986),
                "ignore_errors" => true,
                "protocol_version" => "1.2",
            ]
        ]);
        $newkey = file_get_contents(self::API_ACCESSKEY, false, $context);
        $newkey = json_decode($newkey, true);
        if (empty($newkey) || empty($newkey["access_token"]) || empty($newkey["expires_in"]) || empty($newkey["refresh_token"])) {
            abort(500, "Couldn't accquire access_key");
        }
        $newkey["expires_in"] = Carbon::now()->addSeconds($newkey["expires_in"])->format("d.m.Y H:i:s");
        $this->storeAccessToken($newkey);

        // Store the new key at live website 
        if (config("app.env") !== "production") {
            // Try to fetch an active access_key from Live Instance
            $url = "https://direct.metager.de/access-token";
            $context = stream_context_create([
                "http" => [
                    "method" => "POST",
                    "header" => [
                        "Authorization: Bearer " . config("admitad.website_token"),
                    ],
                    "content" => json_encode($newkey),
                    "ignore_errors" => true,
                ]
            ]);
            file_get_contents($url, false, $context);
        }
    }

    public function storeAccessToken($newkey)
    {
        $redis = Redis::connection(config("cache.stores.redis.connection"));
        $redis->pipeline(function ($pipe) use ($newkey) {
            $pipe->hset("admitad", "access_token", $newkey["access_token"]);
            $pipe->hset("admitad", "refresh_token", $newkey["refresh_token"]);
            $pipe->hset("admitad", "expires_in", $newkey["expires_in"]);
            $expirationSeconds = Carbon::now()->diffInSeconds(Carbon::createFromFormat("d.m.Y H:i:s", $newkey["expires_in"]));
            $pipe->expire("admitad", $expirationSeconds);
            $pipe->del(self::API_REFRESH_KEY_REDIS);
        });
    }

    public function getTeleportLink($adspace, $host, $url, $countrycode, $ip, $useragent, $referer)
    {
        $result = Redis::hgetall($adspace . "." . $host);
        if (empty($result)) {
            // Either this URL is not an affiliate anymore or Redis failed
            // Just redirect to the target URL
            return $url;
        }

        // Generate a Deeplink
        $auth = $this->getAccessKey();
        if (empty($auth)) {
            return $url;
        }

        $apiUrl = self::API_DEEPLINKGENERATOR_URL;
        $adspaceId = self::AD_SPACES[$adspace];
        $campaignId = $result["id"];

        $apiUrl = str_replace("{w_id}", $adspaceId, $apiUrl);
        $apiUrl = str_replace("{c_id}", $campaignId, $apiUrl);

        $params = [
            "subid" => $adspace,
            "ulp" => $url
        ];

        $apiUrl .= "?" . http_build_query($params, "", "&", PHP_QUERY_RFC3986);

        $context = stream_context_create([
            "http" => [
                "method" => "GET",
                "header" => [
                    "Authorization: Bearer " . $auth["access_token"]
                ]
            ]
        ]);

        try {
            $result = file_get_contents($apiUrl, false, $context);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $url;
        }
        $result = json_decode($result, true);
        $redirUrl = "";
        if (empty($result) || !is_array($result) || sizeof($result) <= 0 || !filter_var($result[0], FILTER_VALIDATE_URL)) {
            return $url;
        } else {
            $redirUrl = $result[0];
        }

        // We do have an affiliate Link. Now we need to generate and call the Teleport URL
        $apiUrl = str_replace(["/g/", "/goto/"], "/tptv2/", $redirUrl);

        if (!strpos($apiUrl, "?")) {
            $apiUrl .= "?";
        } else {
            $apiUrl .= "&";
        }
        $apiUrl .= http_build_query([
            "user_agent" => $useragent,
            "referer" => $referer,
            "ip_addr" => $ip,
            "country_code" => $countrycode
        ]);

        // And call it
        $result = file_get_contents($apiUrl, false, $context);
        $result = json_decode($result, true);
        if (empty($result) || !is_array($result) || sizeof($result) <= 0 || !filter_var($result[0], FILTER_VALIDATE_URL)) {
            return $redirUrl;
        }
        return $result[0];
    }

    /**
     * This function will try to get a lock for refreshing the access_key
     */
    private function getRefreshKeyLock()
    {
        $redis = Redis::connection(config("cache.stores.redis.connection"));
        if (empty($redis->setnx(self::API_REFRESH_KEY_REDIS, 1))) {
            return false;
        } else {
            $redis->expire(self::API_REFRESH_KEY_REDIS, 10);
            return true;
        }
    }

    public static function parseHost($url)
    {

        $twoTLDS = [
            "co.uk",
            "com.tr",
        ];

        $host = parse_url($url, PHP_URL_HOST);

        if (empty($host)) {
            return null;
        }

        $hostArray = explode(".", $host);

        if (empty($hostArray)) {
            return null;
        }

        $result = null;
        if (sizeof($hostArray) >= 3) {
            $twotldcheck = $hostArray[sizeof($hostArray) - 2] . "." . $hostArray[sizeof($hostArray) - 1];
            if (in_array($twotldcheck, $twoTLDS)) {
                $result = $hostArray[sizeof($hostArray) - 3] . "." . $hostArray[sizeof($hostArray) - 2] . "." . $hostArray[sizeof($hostArray) - 1];
            } else {
                $result = $hostArray[sizeof($hostArray) - 2] . "." . $hostArray[sizeof($hostArray) - 1];
            }
        } else if (sizeof($hostArray) >= 2) {
            $result = $hostArray[sizeof($hostArray) - 2] . "." . $hostArray[sizeof($hostArray) - 1];
        } else {
            return null;
        }
        return $result;
    }
}
