<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Models\Admitad;
use Carbon\Carbon;
use Log;

class UpdateAffiliatePrograms extends Command
{
    const REFRESH_LOCK = "admitad.refresh-programs";
    const UPDATE_EVERY_MINUTES = 60;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admitad:refresh-programs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches a fresh list of active Affiliate Programs for our adspaces';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Admitad $admitad)
    {
        // Check if data has to get updated
        $lastFetchFile = storage_path("adspaces/last-fetch");
        if (file_exists($lastFetchFile)) {
            $lastFetch = Carbon::createFromFormat("d.m.Y H:i:s", file_get_contents($lastFetchFile));
            if (Carbon::now()->diffInMinutes($lastFetch) < self::UPDATE_EVERY_MINUTES) {
                return 0;
            }
        }

        $cache = Redis::connection(config("cache.stores.redis.connection"));
        if (empty($cache->setnx(self::REFRESH_LOCK, 1))) {
            return 0;
        } else {
            $cache->expire(self::REFRESH_LOCK, 60);
        }

        $programs = $admitad->fetchPrograms();

        if (empty($programs)) {
            return 1;
        }

        // Removes all existing Programs on disk
        $this->clearOldPrograms();
        foreach ($programs as $adspace => $adPrograms) {
            foreach ($adPrograms as $host => $data) {
                // Store the new Data on disk
                $file = storage_path("adspaces/" . $data["id"] . "/program.json");
                $dir = dirname($file);
                if (!file_exists($dir)) {
                    if (!mkdir($dir, 0777, true)) {
                        Log::error("Couldn't write to $dir.");
                        return 1;
                    }
                }
                if (!is_writable($dir)) {
                    Log::error("Couldn't write to $dir.");
                    return 1;
                }
                file_put_contents($file, json_encode($data, JSON_PRETTY_PRINT));
                chmod($file, 0777);

                # Custom config from us
                $configFile = dirname($file) . "/config.json";
                $config = null;
                if (file_exists($configFile)) {
                    $config = json_decode(file_get_contents($configFile), true);
                    if (!in_array($adspace, $config["adspaces"])) {
                        $config["adspaces"][] = $adspace;
                    }
                } else {
                    $config = [
                        "adspaces" => [
                            $adspace,
                        ],
                    ];
                }
                file_put_contents($configFile, json_encode($config, JSON_PRETTY_PRINT));
                chmod($configFile, 0777);
            }
        }

        file_put_contents($lastFetchFile, Carbon::now()->format("d.m.Y H:i:s"));

        Log::info("Refreshed Affiliate Programs");
        return 0;
    }

    private function clearOldPrograms()
    {
        $program_path = storage_path("adspaces/");
        if (!is_dir($program_path)) {
            return;
        }
        $objects = scandir($program_path);
        foreach ($objects as $index => $name) {
            if (in_array($name, ["last-fetch", ".", ".."])) {
                continue;
            }
            if (filetype($program_path . $name) === "dir") {
                $this->rm_dir($program_path . $name);
            } else {
                unlink($program_path . $name);
            }
        }
    }

    private function rm_dir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        $this->rm_dir($dir . "/" . $object);
                    else unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}
