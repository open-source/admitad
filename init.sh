#!/bin/sh

# Exit on error
set -e

if [ -f /root/.env ];
then
  cp /root/.env /html/.env
fi

# Start Cron Service
cron -L /dev/stderr

chmod -R 777 storage/ bootstrap/cache

if [ ! -f .env ]
then
  cp .env.example .env
  chmod 777 .env
  php artisan key:generate
fi

php artisan optimize

# SQLite Database setup
> /html/database/users.sqlite
chmod 0777 /html/database
chmod 0777 /html/database/users.sqlite

# Seed the Database
su -s /bin/bash -c "php artisan admitad:load-programs" www-data
su -s /bin/bash -c "php artisan ip2location:fetch" www-data
su -s /bin/bash -c "php artisan migrate" www-data
su -s /bin/bash -c "php artisan db:seed" www-data