<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class CheckAffiliates extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $request->json()->all();

        $response = [
            "error" => false,
            "message" => "",
            "result" => [],
        ];

        // Check Bearer Token
        $token = $request->bearerToken();
        $validAdspaces = array_keys(\App\Models\Admitad::AD_SPACES);
        if (empty($token) || $token !== config("admitad.website_token")) {
            $response["error"] == true;
            $response["message"] = "A valid Bearer token is required to access this API";
        } else if (empty($data) || empty($data["urls"]) || !is_array($data["urls"])) {
            $response["error"] = true;
            $response["message"] = "Invalid or no request data found. Make sure you supply a json Array of URLs in the parameter \"urls\".";
        } else if (empty($data["lang"]) || !in_array($data["lang"], $validAdspaces)) {
            $response["error"] = true;
            $response["message"] = "Parameter \"lang\" missing. Please set it to either \"de\" or \"en\".";
        } else {
            $this->checkAffiliates($data, $response);
        }

        $status = 200;
        if ($response["error"]) {
            $status = 400;
        }
        return response()->json($response, $status);
    }

    private function checkAffiliates($data, &$response)
    {
        $adspace = $data["lang"];

        $result = [];
        foreach ($data["urls"] as $url) {
            // Only TLD
            $host = \App\Models\Admitad::parseHost($url);

            if (empty($host)) {
                $response["error"] = true;
                $response["message"] = "The url \"$url\" seems to be malformed";
                return;
            }

            $redisAfResult = Redis::hgetall($adspace . "." . $host);
            if (!empty($redisAfResult)) {
                // Check if either black or whitelist is defined
                $blacklist = json_decode($redisAfResult["blacklist"], true);
                $whitelist = json_Decode($redisAfResult["whitelist"], true);

                $whitelisted = $this->checkWhiteList($whitelist, $url);
                $blacklisted = $this->checkBlacklist($blacklist, $url);

                if ($whitelisted && !$blacklisted) {
                    $b64Url = base64_encode($url);
                    $password = hash_hmac("sha256", $b64Url, config("admitad.website_token"));
                    $result[] = [
                        "originalUrl" => $url,
                        "redirUrl" => route("redirector", ["space" => $data["lang"], "url" => $b64Url, "password" => $password]),
                        "image" => $redisAfResult["image"],
                    ];
                } else {
                    $result[] = [
                        "originalUrl" => $url,
                        "redirUrl" => "",
                        "image" => "",
                    ];
                }
            } else {
                $result[] = [
                    "originalUrl" => $url,
                    "redirUrl" => "",
                    "image" => "",
                ];
            }
        }
        $response["result"] = $result;
    }

    private function checkBlacklist($blacklist, $url)
    {
        $blacklisted = false;
        if (!empty($blacklist)) {
            foreach ($blacklist as $blacklistItem) {
                $requiredParts = parse_url($blacklistItem);
                $requiredParameters = [];
                $givenParts = parse_url($url);
                $givenParameters = [];
                // Query Parameter required but none given
                if (!empty($requiredParts["query"]) && !empty($givenParts["query"])) {
                    parse_str($requiredParts["query"], $requiredParameters);
                    parse_str($givenParts["query"], $givenParameters);
                    foreach ($requiredParameters as $key => $value) {
                        if (empty($givenParameters[$key]) || strtolower($givenParameters[$key]) !== strtolower($value)) {
                            // Required Parameter was not found or does not match
                            continue 2;
                        }
                    }
                }
                // Host has to match anytime
                if (
                    $requiredParts["host"] !== $givenParts["host"] ||
                    (!empty($requiredParts["path"]) && $requiredParts["path"] !== $givenParts["path"])
                ) {
                    continue;
                }
                $blacklisted = true;
                break;
            }
        }

        return $blacklisted;
    }

    private function checkWhiteList($whitelist, $url)
    {
        $whitelisted = false;
        if (!empty($whitelist)) {
            foreach ($whitelist as $whitelistItem) {
                $requiredParts = parse_url($whitelistItem);
                $requiredParameters = [];
                $givenParts = parse_url($url);
                $givenParameters = [];
                // Query Parameter required but none given
                if (!empty($requiredParts["query"]) && empty($givenParts["query"])) {
                    continue;
                }
                if (!empty($requiredParts["query"])) {
                    parse_str($requiredParts["query"], $requiredParameters);
                    parse_str($givenParts["query"], $givenParameters);
                    foreach ($requiredParameters as $key => $value) {
                        if (empty($givenParameters[$key]) || strtolower($givenParameters[$key]) !== strtolower($value)) {
                            // Required Parameter was not found or does not match
                            continue 2;
                        }
                    }
                }
                // Host has to match anytime
                if (
                    $requiredParts["host"] !== $givenParts["host"] ||
                    (!empty($requiredParts["path"]) && $requiredParts["path"] !== $givenParts["path"])
                ) {
                    continue;
                }
                $whitelisted = true;
                break;
            }
        } else {
            $whitelisted = true;
        }
        return $whitelisted;
    }
}
