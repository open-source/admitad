<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Models\Admitad;

class Redirector extends Controller
{
    const LOG_PREFIX = "[Redirector]: ";

    public function redirect(Request $request, Admitad $admitad)
    {
        $adspace = $request->input("space", "");
        $url = $request->input("url", "");
        $password = $request->input('password', "");

        if (empty($url) || empty($password)) {
            return response("Either the Url or the password is incorrect", 400);
        }

        // Validate Password
        $expectedPassword = hash_hmac("sha256", $url, config("admitad.website_token"));
        if (!hash_equals($expectedPassword, $password)) {
            return response("Either the Url or the password is incorrect", 400);
        }

        // Validate URL. It's base64 encoded
        $url = base64_decode($url, true);
        if (empty($url) || filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            return response("Supplied URL ($url) seems to be invalid", 400);
        }

        // Validate Adspace
        $validAdspaces = array_keys(\App\Models\Admitad::AD_SPACES);
        if (!in_array($adspace, $validAdspaces)) {
            return response("Supplied adspace ($adspace) is not valid.", 400);
        }

        $host = \App\Models\Admitad::parseHost($url);

        if (empty($host)) {
            Log::error(self::LOG_PREFIX . "Couldn't parse the URL: $url");
            return redirect($url);
        }

        // Generate Teleport Link or if it fails return the url
        // Get the Country Code of the User
        $countrycode = strtoupper($adspace);
        if ($countrycode === "EN") {
            $countrycode = "US";
        }
        $ip = $request->ip();

        $db = null;
        try {
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $db = new \IP2Location\Database(storage_path('app/public/ip2location/IPV4.BIN'), \IP2Location\Database::FILE_IO);
            } elseif (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                $db = new \IP2Location\Database(storage_path('app/public/ip2location/IPV6.BIN'), \IP2Location\Database::FILE_IO);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        if (!empty($db)) {
            $records = $db->lookup($ip, \IP2Location\Database::COUNTRY_CODE);
            if (!empty($records) && preg_match("/^[a-zA-Z]{2}$/", $records)) {
                $countrycode = $records;
            }
        }

        $ip = $request->ip();
        $user_agent = $request->header('user-agent');
        $referer = $request->header("referer");

        $url = $admitad->getTeleportLink($adspace, $host, $url, $countrycode, $ip, $user_agent, $referer);
        Log::info(self::LOG_PREFIX . "Successfull redirect to: $url");
        return redirect($url);
    }
}
