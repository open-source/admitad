<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $program["name"] }}</title>
    <style>
        form {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-template-rows: auto auto;
            gap: 8px 16px;
            grid-template-areas:
                "blacklist whitelist"
                "submit submit";
            margin-bottom: 16px;
        }
        @media(max-width: 700px) {
            form {
                grid-template-columns: 1fr;
                grid-template-rows: auto auto auto;
                grid-template-areas: 
                    "blacklist"
                    "whitelist"
                    "submit";
            }
        }
        form > div {
            display: flex;
            flex-direction: column;
            min-width: 350px;
        }
        form > div > label {
            text-align: center;
        }
    </style>
</head>
<body>
    <main>
        <h1>{{ $program["name"] }}</h1>
        <form action="" method="post">
            <input type="hidden" name="id" value="{{$program["id"]}}">
            <div style="grid-area: blacklist">
                <label for="blacklist">Blacklisted URLs</label>
                <textarea name="blacklist" id="blacklist" cols="30" rows="10">{{implode(PHP_EOL, $config["blacklist"])}}</textarea>
            </div>
            <div style="grid-area: whitelist">
                <label for="whitelist">Whitelisted URLs</label>
                <textarea name="whitelist" id="whitelist" cols="30" rows="10">{{implode(PHP_EOL, $config["whitelist"])}}</textarea>
            </div>
            <input type="submit" value="Speichern" style="grid-area: submit">
        </form>
        <div>
            <table>
                <tbody>
                    @foreach($program as $key => $value)
                    @if(!is_array($value))
                    <tr>
                        <td>{{$key}}</td>
                        <td>{!!$value!!}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </main>
</body>
</html>