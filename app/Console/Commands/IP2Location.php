<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Log;
use ZipArchive;

class IP2Location extends Command
{

    const LOCK_KEY = "ip2location_lock";
    const LOG_PREFIX = "[IP2Location-Cron]: ";
    const IP2LOCATION_BASE_FOLDER = "app/public/ip2location";
    const LAST_FETCH_META_DATA = "app/public/ip2location/meta";
    const LAST_FETCH_META_DATA_FORMAT = "Y-m-d H:i:s";
    const IPV4_PATH = "app/public/ip2location/IPV4.BIN";
    const IPV6_PATH = "app/public/ip2location/IPV6.BIN";
    const MINIMUM_HOURS_BETWEEN_FETCH = 24;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip2location:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches fresh DBs to map ips to country codes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Print an error if directory for data is not writable
        if ((!file_exists(storage_path(self::IP2LOCATION_BASE_FOLDER)) && !is_writable(storage_path("app/public"))) ||
            (file_exists(storage_path(self::IP2LOCATION_BASE_FOLDER)) && !is_writable(storage_path(self::IP2LOCATION_BASE_FOLDER)))
        ) {
            Log::error(self::LOG_PREFIX . "Cannot write to directory " . storage_path(self::IP2LOCATION_BASE_FOLDER));
            return 1;
        }

        // Prevent multiple parallel executions
        $cache = Redis::connection(config("cache.stores.redis.connection"));
        if (empty($cache->setnx(self::LOCK_KEY, 1))) {
            return 0;
        } else {
            $cache->expire(self::LOCK_KEY, 1); // TODO set to 30
        }

        // Break if there wasn't enough time since the last fetch
        if (file_exists(storage_path(self::LAST_FETCH_META_DATA))) {
            $contents = file_get_contents(storage_path(self::LAST_FETCH_META_DATA));
            if (empty($contents)) {
                Log::error(self::LOG_PREFIX . "Couldn't read metadata file.");
                return 1;
            }
            $lastFetch = Carbon::createFromFormat(self::LAST_FETCH_META_DATA_FORMAT, $contents);
            if (empty($lastFetch)) {
                Log::error(self::LOG_PREFIX . "Couldn't parse last fetch date in metadata file ($lastFetch)");
                return 1;
            }

            $diffHours = Carbon::now()->diffInHours($lastFetch);
            if ($diffHours < self::MINIMUM_HOURS_BETWEEN_FETCH) {
                return 0;
            }
        }


        if (!file_exists(storage_path(self::IP2LOCATION_BASE_FOLDER))) {
            mkdir(storage_path(self::IP2LOCATION_BASE_FOLDER), 0777, true);
        }

        if (!$this->downloadZipAndExtractBin(config("admitad.geoip.v4_download"), storage_path(self::IPV4_PATH))) {
            return 1;
        }
        if (!$this->downloadZipAndExtractBin(config("admitad.geoip.v6_download"), storage_path(self::IPV6_PATH))) {
            return 1;
        }

        // Write the current Date to the metadata file
        file_put_contents(storage_path(self::LAST_FETCH_META_DATA), Carbon::now()->format(self::LAST_FETCH_META_DATA_FORMAT));

        $cache->del(self::LOCK_KEY);
        return 0;
    }

    private function downloadZipAndExtractBin($url, $dest)
    {
        // Fetch a fresh IPv4 Database
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            Log::error(self::LOG_PREFIX . "$url is not a valid URL");
            return false;
        }
        // Download ZIP to TMP file
        $tmpFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "tmp.zip";
        file_put_contents($tmpFile, file_get_contents($url));

        // Extract the binfile from archive
        $zip = new ZipArchive;
        $res = $zip->open($tmpFile);
        if ($res === true) {
            for ($i = 0; $i < $zip->numFiles; ++$i) {
                $path = $zip->getNameIndex($i);
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                if (!preg_match("/BIN/", $ext)) {
                    continue;
                }
                copy("zip://{$tmpFile}#{$path}", "$dest");
                break;
            }
        } else {
            Log::error(self::LOG_PREFIX . "Error Downloading $url");
            return false;
        }
        $zip->close();
        unlink($tmpFile);
        return true;
    }
}
