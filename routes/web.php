<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CheckAffiliates;
use App\Http\Controllers\Redirector;
use Illuminate\Http\Request;
use App\Http\Controllers\AdspaceAdmin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Adspace Admin
// Allows to configure Blacklist/Whitelist and List Active Affiliate Programs
Route::get("/", [AdspaceAdmin::class, "show"])->middleware("auth.basic");


Route::get('/healthz', function () {
    return response("Alive");
});
Route::get('r', [Redirector::class, "redirect"])->name("redirector");
Route::post('/check', CheckAffiliates::class);

Route::get('/{id}', [AdspaceAdmin::class, "showId"])->name('adspaceadmin-id')->middleware("auth.basic");
Route::post('/{id}', [AdspaceAdmin::class, "updateId"])->middleware("auth.basic");

// Access Token management is difficult as we can only register a single application
// That's why we need methods for sharing the Access Key
Route::get('access-token', function (\App\Models\Admitad $admitad, Request $request) {
    $token = $request->bearerToken();
    $response = [
        "error" => false,
        "message" => "",
        "result" => null
    ];

    if (empty($token) || $token !== config("admitad.website_token")) {
        $response = [
            "error" => true,
            "message" => "Valid Authorization required.",
            "result" => null,
        ];
    } else {
        $response["result"] = $admitad->getAccessKey();
    }

    return response()->json($response, $response["error"] ? 401 : 200);
});

Route::post('access-token', function (\App\Models\Admitad $admitad, Request $request) {
    $token = $request->bearerToken();
    $data = $request->json()->all();

    $response = [
        "error" => false,
        "message" => "",
        "result" => null
    ];

    if (empty($token) || $token !== config("admitad.website_token")) {
        $response["error"] = true;
        $response["message"] = "Valid Authorization required.";
    } else if (empty($data) || empty($data["access_token"]) || empty($data["refresh_token"]) || empty($data["expires_in"])) {
        $response["error"] = true;
        $response["message"] = "Request data invalid.";
    } else {
        $admitad->storeAccessToken($data);
    }

    return response()->json($response, $response["error"] ? 401 : 200);
});
