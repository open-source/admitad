<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdspaceAdmin extends Controller
{
    public function show(){

        $programs = [];

        $programDir = storage_path("adspaces");
        if(file_exists($programDir)){
            $it = new \RecursiveDirectoryIterator($programDir);
            $display = "program.json";
            foreach(new \RecursiveIteratorIterator($it) as $file){
                if(basename($file) !== $display){
                    continue;
                }
                $program = file_get_contents($file);
                $program = json_decode($program, true);
                if(empty($program)){
                    continue;
                }
                $image = "https://metager.de/meta/picture?" . http_build_query(["url" =>$program["image"]], "", "&", PHP_QUERY_RFC3986);
                $programs[] = [
                    "id" => $program["id"],
                    "name" => $program["name"],
                    "url" => $program["site_url"],
                    "image" => $image,
                ];
            }
        }

        return view('adspaceadmin', ["programs" => $programs]);
    }

    public function showId($id){
        $programBasePath = storage_path("adspaces/$id");
        if(!file_exists($programBasePath)){
            abort(404);
        }

        $program = json_decode(file_get_contents($programBasePath . "/program.json"), true);
        $config = json_decode(file_get_contents($programBasePath . "/config.json"), true);
        if(empty($config["blacklist"])){
            $config["blacklist"] = [];
        }
        if(empty($config["whitelist"])){
            $config["whitelist"] = [];
        }
        if(empty($program) || empty($config)){
            abort(404);
        }

        return view("adspaceadminprogram", ["program" => $program, "config" => $config]);
    }

    public function updateId(Request $request){
        $id = $request->input('id', '');
        $blacklist = trim($request->input('blacklist', ''));
        $whitelist = trim($request->input('whitelist', ''));

        $blacklistArray = [];
        $whitelistArray = [];
        if(!empty($blacklist)){
            $blacklistArray = preg_split('/\r\n|[\r\n]/', $blacklist);
        }
        if(!empty($whitelist)){
            $whitelistArray = preg_split('/\r\n|[\r\n]/', $whitelist);
        }

        foreach($blacklistArray as $blacklistItem){
            if(!filter_var($blacklistItem, FILTER_VALIDATE_URL)){
                return redirect(url()->full());
            }
        }
        foreach($whitelistArray as $whitelistItem){
            if(!filter_var($whitelistItem, FILTER_VALIDATE_URL)){
                return redirect(url()->full());
            }
        }

        // All Data OK
        $configPath = storage_path("adspaces/$id/config.json");

        if(!file_exists($configPath)){
            return redirect(url()->full());
        }

        $config = json_decode(file_get_contents($configPath), true);

        if(empty($config)){
            return redirect(url()->full());
        }

        $config["blacklist"] = $blacklistArray;
        $config["whitelist"] = $whitelistArray;

        file_put_contents($configPath, json_encode($config, JSON_PRETTY_PRINT));

        return redirect(url()->full());
    }
}
