<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class LoadAffiliatePrograms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admitad:load-programs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loads the Affiliate Data from Disk and puts it into Redis for quicker access.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $programDir = storage_path("adspaces");
        if(!file_exists($programDir)){
            return 0;
        }
        $it = new \RecursiveDirectoryIterator($programDir);
        $display = "program.json";
        foreach(new \RecursiveIteratorIterator($it) as $file){
            if(basename($file) !== $display){
                continue;
            }
            $content = json_decode(file_get_contents($file), true);
            if(empty($content)){
                continue;
            }
            $config = json_decode(file_get_contents(dirname($file) . "/config.json"), true);
            $adspaces = [];
            if(empty($config)){
                continue;
            }else{
                $adspaces = $config["adspaces"];
            }
            $host = \App\Models\Admitad::parseHost($content["site_url"]);
            $image = $content["image"];
            $id = $content["id"];
            $url = $content["site_url"];
            $blacklist = empty($config["blacklist"]) ? [] : $config["blacklist"];
            $whitelist = empty($config["whitelist"]) ? [] : $config["whitelist"];
            foreach($adspaces as $adspace){
                Redis::pipeline(function($pipe) use($adspace, $host, $image, $id, $url, $blacklist, $whitelist){
                    $pipe->hset($adspace . "." . $host, "image", $image);
                    $pipe->hset($adspace . "." . $host, "url", $url);
                    $pipe->hset($adspace . "." . $host, "id", $id);
                    $pipe->hset($adspace . "." . $host, "blacklist", json_encode($blacklist));
                    $pipe->hset($adspace . "." . $host, "whitelist", json_encode($whitelist));
                    $pipe->expire($adspace . "." . $host, 7200);
                });
            }
        }
       
        return 0;
    }
}
